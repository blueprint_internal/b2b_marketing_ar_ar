﻿1
00:00:00,000 --> 00:00:02,300
[Facebook]

2
00:00:02,300 --> 00:00:05,482
[Business-Marketing heute ist nicht nur
mobile-first-, sondern auch personenbasiert.]

3
00:00:20,160 --> 00:00:21,688
Herzlich willkommen bei Snagit,

4
00:00:21,688 --> 00:00:26,177
einem umfassenden Tool zum Aufnehmen
von Bildern und Videos auf dem Computer.

5
00:00:26,177 --> 00:00:28,656
Klicke und ziehe und nimm so den gewünschten Content auf.

6
00:00:31,936 --> 00:00:35,498
[Millionen Menschen nutzen heute Facebook,
darunter auch viele Entscheidungsträger.]

7
00:00:49,035 --> 00:00:51,660
[Sie entdecken relevanten Content.]

8
00:01:06,304 --> 00:01:09,441
[B2B-Unternehmen steigern mit mobilen
Lösungen ihre Leads und Umsätze.]

9
00:01:36,125 --> 00:01:39,733
[Schließe dich führenden B2B-Marketern an und
nutze auch du Facebook, um dein Unternehmen voranzubringen.]

10
00:01:39,733 --> 00:01:43,364
[Facebook]

