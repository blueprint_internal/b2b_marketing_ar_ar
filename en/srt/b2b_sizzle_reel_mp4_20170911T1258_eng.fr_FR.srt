﻿1
00:00:00,000 --> 00:00:02,300
[facebook]

2
00:00:02,300 --> 00:00:05,482
[À l’heure actuelle, le marketing ne privilégie pas
uniquement les appareils mobiles ; il est basé sur les personnes.]

3
00:00:20,160 --> 00:00:21,688
Voici Snagit,

4
00:00:21,688 --> 00:00:26,177
un outil tout-en-un pour prendre des images
et réaliser des vidéos sur votre ordinateur.

5
00:00:26,177 --> 00:00:28,656
Cliquez et déplacez le curseur pour sélectionner uniquement ce dont vous avez besoin.

6
00:00:31,936 --> 00:00:35,498
[Des millions de chefs d’entreprise
sont actifs sur Facebook.]

7
00:00:49,035 --> 00:00:51,660
[Ils découvrent des contenus.]

8
00:01:06,304 --> 00:01:09,441
[Les entreprises B2B obtiennent plus de prospects
et de ventes grâce aux solutions mobiles.]

9
00:01:36,125 --> 00:01:39,733
[Qu’attendez-vous pour rejoindre les marketeurs B2B les plus
performants du secteur et vous développer avec Facebook ?]

10
00:01:39,733 --> 00:01:43,364
[facebook]

